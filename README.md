**IMPORTANT ANOTATIONS**

I had several problems with the slowness to consume the services of that api, I don't know if it's a problem with the api, my internet or what... but sometimes consume it really speed and other times needs too much time
There are two branches, develop and develop-candidate.

Develop hasn't got pagination and develop-candidate has pagination, i separated it because with pagination the service goes to slow and sometimes images load and sometimes it doesn't

I've let a video record of the app working.

The app hasn't got a loader implemented.

**Run the application**

First of all i've used Cocoapods, so in the project directory run pod install in the terminal.

After that, in the same route, run swiftgen in terminal to create the Colors, Localization and Assets Files

**Architecture explained**

Used MVVM Architecture with UseCases as Interactors to consume the service, using Coordinator.

**Communications between layers:**

View controller has an instance of viewModel of type ViewModelProtocol to access only this functions instead of all of the viewModel.

ViewModel calls a father function "executeUseCase" who calls a UseCase father function "execute", ¿what is the intention about that?. This comunication has 
made because we need a general place to handle each error that could appear. ViewModel parent has some open functions for the child to handle the success or the error.

UseCase has a similiar behaviour as ViewModel, parent function execute call a child function executeUseCase who calls a baseService parent function, this parent function is also generic and handle posible service errors. It has funtion to handle statusCode validations, to handle especific service statusCode... And child just consume the serive using an observer patron, with is consumed in the baseService, with Alamofire.

The Service response is parsed into ServiceResponse class who has a variable to know if the service was success, a nullable variable to take the posible text error and the dataOut.

BaseUseCase take that Service Response and parse the dataOut to a BussinesModel entity to handle posible changes in the service dataOut to not produce a crash in our application

¿How is notice the view from viewmodel to updat itself? I used a NativeObservable, another observer patron, to notify that the view have to get updated, to avoid a big switch managing each posible observer change, i used states to communicate the view. Each state override a setUp function who delegates (With the ViewManager) the view to update what he needs.


I've added two images explaining with a diagram classes the way that layers communicate it selfs and how is the view push another view with the coordinator.
